/* eslint-disable react/jsx-no-undef */
// routes
import Router from './routes';
// theme
import ThemeProvider from './theme';
// components
import ScrollToTop from './components/scroll-to-top';
import { StyledChart } from './components/chart';
import { AuthProvider } from './utils/authProvider';
import AuthenticationContextProvider from './hooks/hook'

// ----------------------------------------------------------------------

export default function App() {
  return (
    <AuthProvider>
      <AuthenticationContextProvider>
        <ThemeProvider>
          <ScrollToTop />
          <StyledChart />
          <Router />
        </ThemeProvider>
      </AuthenticationContextProvider>
    </AuthProvider>

  );
}
