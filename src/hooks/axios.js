/* eslint-disable prettier/prettier */
import axios from 'axios';

const BASE_URL = 'http://127.0.0.1:3002';
export const axiosPrivate = axios.create({
    baseURL: BASE_URL,
    headers: {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Headers": "Content-Type",
        "Access-Control-Allow-Origin": "*"
    },
    withCredentials: false
});