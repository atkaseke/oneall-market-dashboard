/* eslint-disable import/order */
/* eslint-disable import/named */
/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-async-promise-executor */
/* eslint-disable arrow-body-style */
/* eslint-disable object-shorthand */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
import { axiosPrivate, axiosAuthAPI } from './axios';
import { useReducer, useContext, createContext } from "react";
import secureLocalStorage from "react-secure-storage";

import { useNavigate } from "react-router-dom";
import { ToastProvider, useToasts } from 'react-toast-notifications';



const initialState = {
    validateRegistrantData: {},
    registrantPayload: {},
    token: {},
    data: {},
    createdUserData: {},
    error: {},
    loaded: false,
    currentUser: {},
    userExistData: {},

};


export const useAthentication = () => {
    const { state, dispatch } = useContext(AuthenticationContext);
    const navigate = useNavigate();
    const { addToast } = useToasts();



    const chechAuthUserExist = async (param) => {
        return new Promise(async (resolve, reject) => {
            const registrantExists = await axiosAuthAPI.get(`/user/validate/${param.registrantNumber}`);
            if (registrantExists.status === 200) {
                resolve(registrantExists.data)
            } else {
                reject("error");
            }
        });
    }




    const checkRegistrantExists = (param) => {
        return new Promise(async (resolve, reject) => {
            const registrantExists = await axiosPrivate.get(`/api/registrant/information/${param.registrantNumber}`);
            if (registrantExists.status === 200) {
                resolve(registrantExists.data)
            } else {
                reject("error");
            }
        });
    }



    const GetRegistrant = async (registrationNumber) => {
        const userAuthExist = await chechAuthUserExist(registrationNumber);
        if (userAuthExist === true) {
            navigate("/register");
            addToast("Registrant already exists.", { appearance: 'info' });
        } else {
            const user = await checkRegistrantExists(registrationNumber);
            if (user.success === true) {
                const validUser = {
                    firstName: user.responseBody.first_name,
                    lastName: user.responseBody.last_name,
                    registrantNumber: user.responseBody.registration_number,
                    registranId: user.responseBody.id,
                    success: user.success
                }
                if (validUser) {
                    secureLocalStorage.setItem('rg', validUser);
                    navigate("/register-two");
                }

            } else {
                navigate("/register");
                addToast(user.message, { appearance: 'error' });
            }
        }

    }

    const registerRegistrant = async (param) => {
        try {
            const payload = await axiosAuthAPI.post(`/user`, param);
            if (payload.status === 200 && payload.data.success === true) {
                navigate("/login");
                secureLocalStorage.clear();
            }
            if (payload.status === 200 && payload.data.success === false) {
                navigate("/login");
                secureLocalStorage.clear();
                addToast(payload.data.message, { appearance: 'error' });
            }
        } catch (error) {
            dispatch({ type: "ERROR_RESPONSE", error: error })
        }

    }

    const authenticateRegistrant = async (param) => {
        try {
            const payload = await axiosAuthAPI.post(`/login`, param);
            if (payload && payload.data && payload.status === 200 && payload.data.success === true) {
                const payload_next = {
                    success: payload.data.success,
                    token: payload.data.responseBody.accessToken,
                    registranId: payload.data.responseBody.registrantId,
                    registrantNumber: payload.data.responseBody.userName
                }
                if (payload_next) {
                    secureLocalStorage.setItem('t', payload_next);
                    if (secureLocalStorage.getItem("t")) {
                        navigate("/");
                    }
                }
            }
            else if (payload && payload.status === 200 && (payload.data && payload.data.success === false)) {
                navigate("/login");
                addToast(payload.data.message, { appearance: 'error' });


            }
        } catch (error) {
            dispatch({ type: "ERROR_RESPONSE", error: error });
        }

    }



    const logoutRegistrant = () => {
        secureLocalStorage.clear();
        window.location.href = "/login"

    }


    const getCurrentRegistrant = () => { return secureLocalStorage.getItem("t"); }

    const getValidatedRegistrant = () => {
        return secureLocalStorage.getItem("rg");
    }

    return { registerRegistrant, authenticateRegistrant, logoutRegistrant, state, getCurrentRegistrant, GetRegistrant, getValidatedRegistrant }

}




const actions = {
    LOGIN_DATA_RESPONSE: 'LOGIN_DATA_RESPONSE',
    LOGOUT_DATA_RESPONSE: 'LOGOUT_DATA_RESPONSE',
    SAVED_TOKEN_DATA_RESPONSE: 'SAVED_TOKEN_DATA_RESPONSE',
    REGISTRANT_DATA_REQUEST: 'REGISTRANT_DATA_REQUEST',
    ERROR_RESPONSE: 'ERROR_RESPONSE',
    LOADING_RESPONSE: 'LOADING_RESPONSE',
    USER_CREATED_RESPONSE: 'USER_CREATED_RESPONSE',
    CURRENT_USER_RESPONSE: 'CURRENT_USER_RESPONSE',
    USER_EXISTS_RESPONSE: 'USER_EXISTS_RESPONSE',
};

const authenticationContextReducer = (state, action) => {
    switch (action.type) {
        case actions.LOGIN_DATA_RESPONSE:
            return {
                ...state,
                data: action.data
            }
        case actions.SAVED_TOKEN_DATA_RESPONSE:
            return {
                ...state, token: action.token
            }
        case actions.LOADING_RESPONSE:
            return {
                ...state, loaded: action.loaded
            }
        case actions.REGISTRANT_DATA_REQUEST:
            return {
                ...state, validateRegistrantData: action.validateRegistrantData
            }
        case actions.ERROR_RESPONSE:
            return {
                ...state, error: action.error
            }
        case actions.USER_CREATED_RESPONSE:
            return {
                ...state, createdUserData: action.createdUserData
            }
        case actions.CURRENT_USER_RESPONSE:
            return {
                ...state, currentUser: action.currentUser
            }
        case actions.USER_EXISTS_RESPONSE:
            return {
                ...state, userExistData: action.userExistData
            }

        default:
            return {
                ...state
            }
    }
}


export const AuthenticationContext = createContext([]);
export const AuthenticationContextProvider = (props) => {

    const [state, dispatch] = useReducer(authenticationContextReducer, initialState);

    return (
        <AuthenticationContext.Provider value={{ state, dispatch }}>
            {props.children}
        </AuthenticationContext.Provider>
    );
}
export const useAuthenticationContext = () => useContext(AuthenticationContext);



ToastProvider.defaultProps = {
    autoDismiss: true,
    autoDismissTimeout: 2000,
    newestOnTop: false,
    placement: 'top-center',
    transitionDuration: 220
};

